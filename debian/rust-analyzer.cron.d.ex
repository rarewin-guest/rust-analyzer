#
# Regular cron jobs for the rust-analyzer package
#
0 4	* * *	root	[ -x /usr/bin/rust-analyzer_maintenance ] && /usr/bin/rust-analyzer_maintenance
